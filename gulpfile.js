var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    merge = require('merge-stream'),
    gulpif = require('gulp-if'),
    autoprefixer = require('gulp-autoprefixer'),
    mainBower = require('main-bower-files'),
    uglify = require('gulp-uglify'),
    cssMinify = require('gulp-minify-css'),
    gulpFilter = require('gulp-filter'),
    order = require('gulp-order'),
    debug = require('gulp-debug'),
    livereload = require('gulp-livereload'),
    lr = require('tiny-lr'),
    server = lr(),
    colors = require('colors'),
    install = require("gulp-install"),
    ts = require('gulp-typescript'),
    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
    isProduction = env === "production",
    paths = {
        js: {
            vendorConcatTo: 'vendor.js',
            src: ['assets/js/**/*js', 'assets/js/*js'],
            dest: 'public/js',
            customPaths: [
                "assets/vendor/zip.js/WebContent/inflate.js",
                "assets/vendor/zip.js/WebContent/deflate.js"
            ]
        },
        ts: {
            src: ['assets/ts/*ts', 'assets/ts/**/*ts'],
            defDest: 'public/definitions',
            dest: 'public/js',
            concatTo: 'app.js'
        },
        css: {
            vendorConcatTo: 'vendor.css',
            concatTo: 'app.css',
            src: 'assets/css/*css',
            dest: 'public/css'
        },
        scss: {
            src: 'assets/scss/*scss'
        },
        fonts: {
            src: ['assets/vendor/bootstrap/dist/fonts/*',
                'assets/vendor/font-awesome/fonts/*',
                'assets/fonts/*'
            ],
            dest: 'public/fonts'
        },
        images: {
            src: "assets/images/*",
            dest: "public/images"
        },
        tests: {
            mainDest: 'tests/main',
            vendorDest: 'tests/vendor',
            dest: "tests"
        },
        templates: {
            src: 'assets/template/**/*',
            dest: 'public/template'
        }
    };

gulp.task('start', ['packages', 'build', 'watch'], function() {
    var debug = require('debug')('angular-app'),
        app = require('./app'),
        opener = require('opener');

    app.set('port', process.env.PORT || 3000);

    var server = app.listen(app.get('port'), function() {
        var address = 'http://localhost:' + app.get('port');
        opener(address);
        debug('Express server listening on port ' + app.get('port'));
    });
});

gulp.task('buildtest', function() {
    var mocks = gulp.src(mainBower())
        .pipe(gulpFilter(['**/angular-mocks.js']))
        .pipe(gulp.dest(paths.tests.dest));
    var bowerFiles = gulp.src(mainBower())
        .pipe(gulpFilter(['**/*.js', '!**/*.min.js', '!**/angular-mocks.js']))
        .pipe(concat(paths.js.vendorConcatTo))
        .pipe(gulp.dest(paths.tests.vendorDest));
    var tsResult = gulp.src(paths.ts.src)
        .pipe(ts({
            declarationFiles: true,
            noLib: false,
            target: 'ES5',
            module: 'commonjs'
        }));
    var tsFiles = tsResult.js
        .pipe(concat(paths.ts.concatTo))
        .pipe(gulp.dest(paths.tests.mainDest));
    var customFiles = gulp.src(paths.js.customPaths)
        .pipe(gulp.dest(paths.tests.mainDest));
    return merge(bowerFiles, customFiles, mocks, tsFiles);
});

gulp.task('build', ['packages', 'scripts', 'typescript', 'styles', 'fonts', 'images'], function() {});

gulp.task('packages', function() {
    return gulp.src(['bower.json'])
        .pipe(install())
        .pipe(livereload(server));
});

gulp.task('scripts', ['templates'], function() {
    var bowerFiles = gulp.src(mainBower())
        .pipe(gulpFilter(['**/*.js', '!**/*.min.js', '!**/angular-mocks.js']))
        .pipe(gulpif(isProduction, uglify({
            mangle: false
        })))
        .pipe(concat(paths.js.vendorConcatTo))
        .pipe(gulp.dest(paths.js.dest));
    var customFiles = gulp.src(paths.js.customPaths)
        .pipe(gulpif(isProduction, uglify({
            mangle: false
        })))
        .pipe(gulp.dest(paths.js.dest));
    return merge(bowerFiles, customFiles)
            .pipe(livereload(server));
});

gulp.task('typescript', function() {
    var tsResult = gulp.src(paths.ts.src)
        .pipe(ts({
            declarationFiles: true,
            noLib: false,
            target: 'ES5',
            module: 'commonjs'
        }));
    tsResult.dts.pipe(gulp.dest(paths.ts.defDest));
    return tsResult.js
        .pipe(concat(paths.ts.concatTo))
        .pipe(gulpif(isProduction, uglify({
            mangle: false
        })))
        .pipe(gulp.dest(paths.ts.dest))
        .pipe(livereload(server));
});

gulp.task('styles', function() {
    var bowerFiles = gulp.src(mainBower())
        .pipe(gulpFilter(['**/*.css', '!**/ng-table.css']))
        .pipe(concat(paths.css.vendorConcatTo))
        .pipe(gulp.dest(paths.css.dest));
    var assetsCssFiles = gulp.src(paths.css.src);
    var assetsScssFiles = gulp.src(paths.scss.src).pipe(sass());
    var assetsFiles = merge(assetsCssFiles, assetsScssFiles)
        .pipe(concat(paths.css.concatTo))
        .pipe(autoprefixer());
    return merge(assetsFiles, bowerFiles)
        .pipe(gulpif(isProduction, cssMinify({
            keepBreaks: true
        })))
        .pipe(gulp.dest(paths.css.dest))
        .pipe(livereload(server));
});

gulp.task('fonts', function() {
    return gulp.src(paths.fonts.src)
        .pipe(gulp.dest(paths.fonts.dest))
        .pipe(livereload(server));
});

gulp.task('images', function() {
    return gulp.src(paths.images.src)
        .pipe(gulp.dest(paths.images.dest))
        .pipe(livereload(server));
});

gulp.task('templates', function() {
    return gulp.src(paths.templates.src)
        .pipe(gulp.dest(paths.templates.dest));
});

gulp.task('watch', function() {
    var livereloadPort = 35729;
    server.listen(livereloadPort, function(err) {
        console.log(colors.green("Started tiny-lr server at: "), colors.magenta(livereloadPort));
        if (err) {
            return console.log(err);
        }

        gulp.watch(['assets/scss/*scss', 'assets/scss/**/*scss', 'assets/css/*css', 'assets/css/**/*css'], ['styles']);
        gulp.watch(['assets/js/*js', 'assets/js/**/*js'], ['scripts']);
        gulp.watch(['assets/ts/*ts', 'assets/ts/**/*ts'], ['typescript']);
        gulp.watch(['assets/fonts/*'], ['fonts']);
        gulp.watch(['bower.json'], ['build']);
    });
});