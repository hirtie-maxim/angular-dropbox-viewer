angular.module('mock.Services', [])
    .value('dvClientMock', Services.DropviewClient.client)
    .service('dropboxErrorServiceMock', Services.DropboxErrorService)
    .factory('fileReaderServiceMock', ['dvClientMock', 'archiveZipServiceMock', 'dropboxErrorServiceMock', function FileReaderService(dvClient, archiveZipService, dropboxErrorService) {
        var readDropbox = function(path, callback) {
                dvClient.readdir(path || "/", function(error, entries, root, contents) {
                    dropboxErrorService.showError(error);
                    callback && callback(Common.Content.toContents(contents));
                });
            },
            readZip = function(path, callback) {
                var pathToZip = path.endsWith(".zip") ? path : path.substring(0, path.indexOf('.zip') + 4);
                archiveZipService.read(path, function(entries) {
                    callback && callback(entries.map(function(el) {
                        return Common.Content.ZipEntryToContent(el, path);
                    }));
                });
            };
        return function(path, callback) {
            if (dvClient.isAuthenticated()) {
                var isZip = path.contains(".zip");
                if (isZip) {
                    readZip(path, callback);
                } else {
                    readDropbox(path, callback);
                }
            }
        };
    }])
    .factory('archiveZipServiceMock', [ 'dvClientMock', 'dropboxErrorServiceMock', function ArchiveZipService(dvClient, dropboxErrorService) {
            var config = {}, lastZipRead = "", lastZipReadEntries = [], zipEntriesFilterAndReturn = function (path, entries, callback) {
                var matches = /^(.+\.zip)(.+)?$/gi.exec(path), mainPathParts = path.substring(1).split("/"), level = 0;
                if (matches[2]) {
                    mainPathParts = matches[2].substring(1).split("/");
                    level = mainPathParts.length;
                }
                var isFile = !!/.+\/.+\..+$/.test(path), filtered = entries.filter(function (entry) {
                    var pathParts = entry.filename.split("/");
                    if (pathParts.length > 0 && !pathParts[pathParts.length - 1]) {
                        pathParts.length--;
                    }
                    if (!isFile && level == pathParts.length - 1) {
                        for (var i = 0; i < pathParts.length - 1; i++) {
                            if (pathParts[i] !== mainPathParts[i]) {
                                return false;
                            }
                        }
                        ;dropboxErrorServiceMock
                        return true;
                    } else if (isFile && level == pathParts.length) {
                        for (var i = 0; i < pathParts.length; i++) {
                            if (pathParts[i] !== mainPathParts[i]) {
                                return false;
                            }
                        }
                        ;
                        return true;
                    } else {
                        return false;
                    }
                });
                callback && callback(filtered);
            };
            zip.workerScriptsPath = "/js/";
            config.read = function (path, callback) {
                if (dvClient.isAuthenticated()) {
                    var pathToZip = path.endsWith(".zip") ? path : path.substring(0, path.indexOf('.zip') + 4);
                    if (lastZipRead == pathToZip && lastZipReadEntries.length) {
                        zipEntriesFilterAndReturn(path, lastZipReadEntries, callback);
                    } else {
                        dvClient.readFile(pathToZip, {
                            blob: true
                        }, function (error, blob) {
                            dropboxErrorService.showError(error);
                            zip.createReader(new zip.BlobReader(blob), function (reader) {
                                reader.getEntries(function (entries) {
                                    if (entries.length) {
                                        lastZipRead = pathToZip;
                                        lastZipReadEntries = entries;
                                        zipEntriesFilterAndReturn(path, lastZipReadEntries, callback);
                                    }
                                });
                            }, function (error) {
                            });
                        });
                    }
                }
            };
            return config;
        }]);