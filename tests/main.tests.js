'use strict';

describe('AuthorizationTest', function() {
    var scope; //we'll use this scope in our tests
    var dropboxAuthController,
        dropboxFileTableController;
    var $state, $injector, myServiceMock, state = 'dir.dropbox';

    //mock Application to allow us to inject our own dependencies
    beforeEach(angular.mock.module('dropview'));
    //mock the controller for the same reason and include $rootScope and $controller
    beforeEach(angular.mock.inject(function($rootScope, $controller) {
        //create an empty scope
        scope = $rootScope.$new();
        //declare the controller and inject our empty scope
        dropboxAuthController = $controller('DropboxAuthorizationController', {
            $scope: scope
        });
    }));

    beforeEach(angular.mock.inject(function(_$state_, _$injector_, $templateCache) {
        $state = _$state_;
        $injector = _$injector_;

        // We need add the template entry into the templateCache if we ever
        // specify a templateUrl
        $templateCache.put('template.html', '');
    }));

    it('should have method authentication', function() {
        expect(dropboxAuthController.authenticate).toBeDefined();
    });

    it('should respond to URL', function() {
        expect($state.href(state, {
            path: ''
        })).toEqual('#/dropbox');
    });
});

describe('ContentLoadingTest', function() {
    var scope; //we'll use this scope in our tests
    var dropboxAuthController,
        dropboxFileTableController;
    var $state, $injector, myServiceMock, state = 'dir.dropbox';

    //mock Application to allow us to inject our own dependencies
    beforeEach(angular.mock.module('dropview'));
    //mock the controller for the same reason and include $rootScope and $controller
    beforeEach(angular.mock.inject(function($rootScope, $controller) {
        //create an empty scope
        scope = $rootScope.$new();
        //declare the controller and inject our empty scope
        dropboxAuthController = $controller('DropboxAuthorizationController', {
            $scope: scope
        });
    }));

    beforeEach(angular.mock.inject(function(_$state_, _$injector_, $templateCache) {
        $state = _$state_;
        $injector = _$injector_;

        // We need add the template entry into the templateCache if we ever
        // specify a templateUrl
        $templateCache.put('template.html', '');
    }));

    it('should load root folder', function() {
        expect($state.href(state, {
            path: ''
        })).toEqual('#/dropbox');
        // expect(dropboxFileTableController.contents.length).toBeGreaterThan(0);
    });
});
