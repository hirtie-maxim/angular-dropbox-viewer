describe('ServicesDefineTest', function(){
    var dropviewClient,
        fileReaderService,
        archiveZipService,
        dropboxErrorService;
    beforeEach(angular.mock.module('mock.Services'));

    beforeEach(angular.mock.inject(function(dvClientMock, fileReaderServiceMock, archiveZipServiceMock, dropboxErrorServiceMock){
        dropviewClient = dvClientMock;
        fileReaderService = fileReaderServiceMock;
        archiveZipService = archiveZipServiceMock;
        dropboxErrorService = dropboxErrorServiceMock;
    }));

    it('services should be mocked', function(){
        expect(dropviewClient).toBeDefined();
        expect(fileReaderService).toBeDefined();
    });
});