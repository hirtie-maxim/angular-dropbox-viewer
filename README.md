angular-dropbox-viewer
======================
Is an viewer created using angular and dropbox.js. Mostly...
The server-side is based on Node.js and gulp.

To run application open your command-line, and run this commands:

```sh
git clone git@bitbucket.org:hirtie-maxim/angular-dropbox-viewer.git
```
This will clone project to your local path.

```sh
npm install
```
This will install all node packages that are needed for running this app.

```sh
gulp start
```
And this will start application and open it in your default browser.


To run tests:
```sh
npm test
```

Before running the app on heroku, if you wish to push it directly on heroku, run this command:

```sh
heroku config:set NODE_ENV=production
```

By default wercker hooks the push on bitbucket, runs tests and deploies the project.