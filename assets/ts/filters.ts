/// <reference path="typings/tsd.d.ts" />
module Filters {
	export interface ISeconds {
		(input: string): string;
	}

	export function Seconds(): ISeconds {
		return <ISeconds>function(input: string) {
            var value: number = ~~input,
                result: string = '';

            var minutes: number = Math.floor(value / 60);
            value -= minutes * 60;

            var seconds: number = parseInt((value % 60) + '', 10);

            return (minutes < 10 ? '0' + minutes : minutes + '') + ':' + (seconds < 10 ? '0' + seconds : seconds + '');
        };
	}
}