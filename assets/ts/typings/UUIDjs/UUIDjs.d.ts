declare module UUIDjs {
	interface UUID {
		hex: string;
    	version: number;
    	equals(uuid: UUID): boolean;
    	fromParts(timeLow: number, timeMid: number, timeHiAndVersion: number, clockSeqHiAndReserved: number, clockSeqLow: number, node: number): void;
    	toBytes(): number;
    	toString(): string;
    	toURN(): string;
	}

    function create(version?: number): UUID;
}