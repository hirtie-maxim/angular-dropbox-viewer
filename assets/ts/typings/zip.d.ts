declare module zip {
    var useWebWorkers: boolean;
    var workerScriptsPath: string;
    function createReader(reader: zip.Reader, callback: (reader: zip.Reader) => void, onError: Function);
    function createWritter(writter: zip.Writter, callback: (reader: zip.Writter) => void, onError: Function);

	export class Reader {

	}

	export class BlobReader extends Reader {
		constructor(blob: any);
		getEntries(callback: (entries: any[]) => void);
	}

	export class Writter {

	}

	export class BlobWriter extends Writter {
		
	}
}

declare module "zip" {
	export = zip;
}