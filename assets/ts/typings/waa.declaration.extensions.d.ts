declare var mozAudioContext: {
    new (): AudioContext;
}

declare var oAudioContext: {
    new (): AudioContext;
}

declare var msAudioContext: {
    new (): AudioContext;
}