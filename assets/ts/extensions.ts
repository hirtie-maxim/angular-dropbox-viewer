interface String {
	endsWith(suffix: string): boolean;
	contains(subString: string): boolean;
}

interface Array<T> {
	indexOfObject(property: string, value: any): number;
}

if(!String.prototype.endsWith){
	String.prototype.endsWith = function(suffix: string): boolean {
    	return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

if (!String.prototype.contains) {
    String.prototype.contains = function(subString: string): boolean {
        return this.indexOf(this, subString) !== -1;
    };
}

if(!Array.prototype.indexOfObject) {
	Array.prototype.indexOfObject = function(property: string, value: any): number {
    	for (var i = 0, len = this.length; i < len; i++) {
        	if (this[i][property] === value) return i;
    	}
    	return -1;
	}
}