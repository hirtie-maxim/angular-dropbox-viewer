module Configurator {
	export function Configurator($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise("redirect");
        $stateProvider
            .state('dir', {
                abstract: true,
                views: {
                    'dropboxaccount': {
                        templateUrl: 'templates/dropbox_account'
                    },
                    'accountinfo@dir': {
                        templateUrl: 'templates/account_info',
                        controller: 'DropboxAccountInfoController as accountInfo'
                    }
                }
            })
            .state('dir.dropbox', {
                url: '/dropbox{path:.*}',
                views: {
                    'filetable@dir': {
                        templateUrl: 'templates/file_table',
                        controller: 'DropboxFileTableController as filetable',
                        data: {
                            startsWith: "Dropbox",
                            breadcrumbs: true
                        }
                    }
                }
            })
            .state('auth', {
                url: '/redirect*path',
                views: {
                    "dropboxauth": {
                        templateUrl: 'templates/dropbox_auth',
                        controller: 'DropboxAuthorizationController as dropboxAuth'
                    }
                }
            });
	}

	export function StateValidator($rootScope, $state, authService) {
        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
            if (toState.name !== "auth" && !authService.isAuthenticated()) {
                $state.go("auth", {
                    path: toParams.path
                });
                event.preventDefault();
            }

            if (toState.name === "auth") {
                $rootScope.pageTitle = "Dropview - Authentication";
            } else {
                $rootScope.pageTitle = "Dropview - Dropbox" + toParams.path || "Dropview";
            }
        });
    }
}