/// <reference path="typings/tsd.d.ts" />
module App {
	export class Dropview {
		public static init() {
			angular.module('dropview', ['ui.router', 'angularSpinner', 'ngTable', 'ui.bootstrap.modal', 'xeditable', 'nouislider'])
				.config(Configurator.Configurator) 
				.run(Configurator.StateValidator);
			angular.module('dropview')
				.controller('DropboxFileTableController', Controllers.DropboxFileTableController) 
				.controller('DropboxAuthorizationController', Controllers.DropboxAuthorizationController) 
				.controller('DropboxAccountInfoController', Controllers.DropboxAccountInfoController)
				.controller('TextEditorController', Controllers.TextEditorController)
				.controller('ImageViewerController', Controllers.ImageViewerController)
				.controller('AudioPlayerController', Controllers.AudioPlayerController);
			angular.module('dropview')
				.directive('breadcrumbs', Directives.Breadcrumbs) 
				.directive('dropboxActions', Directives.DropboxActions) 
				.directive('dropboxAction', Directives.DropboxAction) 
				.directive('videoPlayer', Directives.VideoPlayer);
			angular.module('dropview')
				.filter('seconds', Filters.Seconds);
			angular.module('dropview')
				.value('dvClient', Services.DropviewClient.client) 
				.service('spinnerService', Services.SpinnerService) 
				.service('dropviewWritter', Services.DropviewWritter) 
				.service('dropviewReader', Services.DropviewReader) 
				.service('dropviewClient', Services.DropviewClient)
				.factory('videoPlayerService', Services.Factories.VideoPlayerService) 
				.factory('audioPlayerService', Services.Factories.AudioPlayerService)
				.service('dropboxErrorService', Services.DropboxErrorService)
				.service('dropboxActionService', Services.DropboxActionService)
				.service('authService', Services.AuthService);

			angular.bootstrap(document, ['dropview']);
		}
	}
}

$(function() {
	App.Dropview.init();
});
