module Controllers {
	export class DropboxFileTableController{
		public contents: Common.Content[] = new Array<Common.Content>();
		public tableParams: any;

		constructor(
			private $scope, 
			private $stateParams, 
			private $filter, 
			private ngTableParams, 
			private usSpinnerService,
			private dropviewReader: Services.DropviewReader) {
			var self = this;

			this.tableParams = new ngTableParams({
				count: 0,
				sorting: {
					name: 'asc'
				}
			}, {
				counts: [],
				getData: function($defer, params) {
					self.orderData(this.contents);
				}
			});
			this.readdir();
		}

		private orderData(data) {
			var orderbyFilter = this.$filter('orderBy'),
				orderBy = this.tableParams.orderBy(),
				sorting = this.tableParams.sorting(),
				isOrderedBykind = !orderBy.indexOf("+kind") || !orderBy.indexOf("-kind");
			if (!isOrderedBykind) {
				var sortBy = Object.keys(sorting)[0],
					direction = (sorting[sortBy] == 'asc' ? '+' : '-')
					orderBy.splice(0, 0, direction + 'kind');
			}
			var orderedData = sorting ? orderbyFilter(data, orderBy) : data;
			this.contents = orderedData;
		}

		private readdir() {
			var self = this;
			if(!/.txt/gi.test(self.$stateParams.path)){
				this.dropviewReader.readDirectory(self.$stateParams.path, function(contents) {
					if (!self.$scope.$$phase) {
						self.$scope.$apply(function() {
							self.orderData(contents);
						});
					} else {
						self.orderData(contents);
					}
					self.usSpinnerService.stop('main-spinner');
				});
			}
		}
	}

	export class DropboxAuthorizationController {
		constructor(
			private authService: Services.AuthService, 
			private $state, 
			private $stateParams) {
			var self = this;
			this.authService.authenticate({
				interactive: false
			}, (client: Dropbox.Client) => {
				if (self.authService.isAuthenticated()) {
					self.$state.go('dir.dropbox', {
						path: self.$stateParams.path || ''
					});
				}	
			});
		}

		public authenticate() {
			var self = this;
			this.authService.authenticate({
				interactive: true
			},(client: Dropbox.Client) => {
				if (self.authService.isAuthenticated()) {
					self.$state.go('dir.dropbox', {
						path: self.$stateParams.path || ''
					});
				}	
			});
		}
	}

	export class DropboxAccountInfoController {
		public name: string = '';
		public email: string = '';
		public quotaInfo: any = {};

		constructor(
			private authService: Services.AuthService,
			private dropviewReader: Services.DropviewReader, 
			private $scope, 
			private $state) {
			this.getAccountInfo();
		}

		private getAccountInfo() {
			var self = this;
			this.dropviewReader.dropboxReader.getAccountInfo((accountInfo: Common.AccountInfo) => {
				self.$scope.$apply(function() {
					self.name = accountInfo.name;
					self.email = accountInfo.email;
					self.quotaInfo = {
						usedInPercents: accountInfo.quotaInfo.usedInPercents,
						overUsed: accountInfo.quotaInfo.overUsed,
						usedQuota: accountInfo.quotaInfo.usedQuota,
						quota: accountInfo.quotaInfo.quota
					};
				});
			});
			$('.dropbox-account').affix({
				offset: {
					top: 40
				}
			});
		}

		public logOut() {
			var self = this;
			this.authService.signOff(function() {
				self.$state.go("auth", {
					path: ''
				});
			});
		}
	}

	export class TextEditorController {
		constructor(
			private $scope,
			private dropviewWritter: Services.DropviewWritter,
			private spinnerService: Services.SpinnerService, 
			private $modalInstance, 
			public text: string, 
			public content: Common.Content) {
		}

		public save() {
			this.dropviewWritter.updateOrCreateTextFile(this.content.path, this.text);
			this.$modalInstance.close();
		}

		public saveText() {
			this.$scope.textEditorForm.$save();
		}

		public cancel() {
			this.$modalInstance.dismiss('cancel');
		}
	}

	export class ImageViewerController {
		public slides: Common.ImageInfo[] = new Array<Common.ImageInfo>();

		constructor(private $modalInstance, public carouselController: any) {
			var self = this;
			this.carouselController.first(self.slides);
		}

		public next(): void {
			var self = this;
			this.carouselController.next(self.slides);
		}

		public prev(): void {
			var self = this;
			this.carouselController.prev(self.slides);
		}
	}

	export class AudioPlayerController {
		private static _audioContext: AudioContext;
		private static _gainNode: GainNode;
		private static _source: AudioBufferSourceNode;
		private static _audioBuffer: AudioBuffer;

		private static _playWhenLoaded: boolean = false;
		private static _pausedAt: number = 0;
		private static _startedAt: number = 0;

		private static _apInterval: number = 0;

		public volume: number = 50;
		public playStatus: string = "stop";
		public isVolumeOff: boolean = false;
		public startAt: number = 0;
		public duration: number = 0;
		public played: number = 0;

		constructor(
			private $scope, 
			private $modalInstance, 
			private spinnerService: Services.SpinnerService,
			private arrayBuffer,
			public info: string) {
			var self = this;
			if(!AudioPlayerController._audioContext) {
				var contextClass = (AudioContext || mozAudioContext || oAudioContext || msAudioContext);
				if(contextClass) {
					AudioPlayerController._audioContext = new contextClass();
					AudioPlayerController._gainNode = AudioPlayerController._audioContext.createGain();
					AudioPlayerController._gainNode.gain.value = this.volume / 100;
				} else {
					throw new Error("Your browser doesn't support Web Audio API.");
				}
			}

			AudioPlayerController._audioContext.decodeAudioData(arrayBuffer, function(buffer) {
				if (!buffer) {
					console.error('error decoding file data!');
					return;
				}
				AudioPlayerController._audioBuffer = buffer;
				self.$scope.$apply(function() {
					self.duration = ~~AudioPlayerController._audioBuffer.duration;
				});
				if (AudioPlayerController._playWhenLoaded) {
					self.$scope.$broadcast('audio-player:autoplay');
				}
				self.spinnerService.stopModal();
			}, function() {
				console.error('decodeAudioData error', arguments);
			});

			$modalInstance.result.then(() => {}, () => {
				self.stop();
			});

			this.registerBroadcast();
		}

		private registerBroadcast() {
			var self = this;
			this.$scope.$on('audio-player:play', function(event) {
				if (!AudioPlayerController._audioBuffer) {
					AudioPlayerController._playWhenLoaded = true;
				} else {
					self.play();
				}
			});

			this.$scope.$on('audio-player:stop', function(event) {
				self.stop();
			});

			this.$scope.$on('audio-player:pause', function(event) {
				self.pause();
			});

			this.$scope.$on('audio-player:autoplay', function() {
				self.play();
			});
		}

		public progressChange(event, value) {
			var self = this;
			if(self.playStatus == "play" && AudioPlayerController._source) {
				AudioPlayerController._pausedAt = 0;
				AudioPlayerController._source.stop(0);
			}
			AudioPlayerController._pausedAt = ~~value * 1000;
			this.$scope.$apply(function() {
				self.played = ~~value;
			});
			this.play();
		}

		public volumeChange(event, value) {
			var fraction = parseInt(value) / 100;
			this.volume = value;
			this.isVolumeOff = this.volume == 0;
			AudioPlayerController._gainNode.gain.value = fraction * fraction;
		}

		public play() {
			var self = this;
			AudioPlayerController._source = AudioPlayerController._audioContext.createBufferSource();
			AudioPlayerController._source.connect(AudioPlayerController._gainNode);
			AudioPlayerController._source.buffer = AudioPlayerController._audioBuffer;
			AudioPlayerController._gainNode.connect(AudioPlayerController._audioContext.destination);
			if (AudioPlayerController._pausedAt) {
				AudioPlayerController._startedAt = Date.now() - AudioPlayerController._pausedAt;
				AudioPlayerController._source.start(0, AudioPlayerController._pausedAt / 1000);
			} else {
				AudioPlayerController._startedAt = Date.now();
				AudioPlayerController._source.start(0);
			}
			clearInterval(AudioPlayerController._apInterval);
			AudioPlayerController._apInterval = setInterval(function() {
				self.$scope.$apply(function() {
					self.played++;
					if(self.played >= self.duration) {
						clearInterval(AudioPlayerController._apInterval);
					}
				});
			}, 1000);
			if (!this.$scope.$root.$$phase) {
				this.$scope.$apply(function() {
					self.playStatus = "play";
				});
			} else {
				self.playStatus = "play";
			}
		}

		public stop() {
			var self = this;
			clearInterval(AudioPlayerController._apInterval);
			AudioPlayerController._pausedAt = 0;
			self.played = 0;
			if (!AudioPlayerController._source) {
				return;
			}
			AudioPlayerController._source.stop(0);
			if (!this.$scope.$root.$$phase) {
				this.$scope.$apply(function() {
					self.playStatus = "stop";
				});
			} else {
				self.playStatus = "stop";
			}
		}

		public pause() {
			var self = this;
			clearInterval(AudioPlayerController._apInterval);
			AudioPlayerController._pausedAt = Date.now() - AudioPlayerController._startedAt;
			if (!AudioPlayerController._source) {
				return;
			}
			AudioPlayerController._source.stop(0);
			if (!this.$scope.$root.$$phase) {
				this.$scope.$apply(function() {
					self.playStatus = "pause";
				});
			} else {
				self.playStatus = "pause";
			}
		}

		public triggerVolume() {
			this.isVolumeOff = !this.isVolumeOff;
			if (this.isVolumeOff) {
				AudioPlayerController._gainNode.gain.value = 0;
			} else {
				var fraction = this.volume / 100;
				AudioPlayerController._gainNode.gain.value = fraction * fraction;
			}
		}
	}
}