/// <reference path="typings/tsd.d.ts" />

module Services {
    import File = Dropbox.File;
    import ApiError = Dropbox.ApiError;

	export class AuthService {
		constructor(private dropviewClient: DropviewClient){}

		isAuthenticated(): boolean {
			return this.dropviewClient.isAuthenticated();
		}

        authenticate(options: Dropbox.AuthenticateOptions, callback: (client: Dropbox.Client) => void) {
            this.dropviewClient.authenticate(options, callback);
        }

        signOff(callback: () => void) {
            this.dropviewClient.signOff(callback);
        }
	}

    export class DropviewWritter {
        constructor(private dropviewClient: DropviewClient) {}

        public updateOrCreateTextFile(path: string, text: string, callback?: () => void) {
            this.dropviewClient.writeFile(path, text, callback);
        }
    }

    export class DropboxReader {
        constructor(private dropviewClient: DropviewClient) {}
        public readDirectory(path: string, callback?: (contents: Common.Content[]) => void) {
            this.dropviewClient.readDirectory(path || "/", (entries: string[], root: File.Stat, contents: File.Stat[]) => {
                callback && callback(Common.Content.toContents(contents));
            });
        }

        public readBlob(path: string, callback?: (blob: Blob) => void) {
            this.dropviewClient.readBlob(path, (blob: Blob, data: File.Stat, rangeInfo: Dropbox.Http.RangeInfo) => {
                callback && callback(blob);
            });
        }

        public readArrayBuffer(path: string, callback?: (buffer: ArrayBuffer) => void) {
            this.dropviewClient.readArrayBuffer(path, (buffer: ArrayBuffer, data: File.Stat, rangeInfo: Dropbox.Http.RangeInfo) => {
                callback && callback(buffer);
            });
        }

        public readFile(path: string, callback: (text: string) => void) {
            this.dropviewClient.readFile(path, (fileContents: string, data: File.Stat) => {
                callback && callback(fileContents);
            });
        }

        public getImageUrl(path: string, callback?: (url: string) => void) {
            this.dropviewClient.getUrl(path, (shareUrl: File.ShareUrl) => {
                callback && callback(shareUrl.url);
            });
        }

        public getImages(path: string, callback?: (contents: Common.Content[]) => void) {
            this.dropviewClient.readDirectory(path, (entries: string[], root: File.Stat, stats: File.Stat[]) => {
                var contents = Common.Content.toContents(stats).filter((content) => {
                    return content.isImage();
                });

                callback && callback(contents);
            });
        }

        public getAccountInfo(callback: (accountInfo: Common.AccountInfo) => void) {
            this.dropviewClient.getAccountInfo((accountInfo: Dropbox.AccountInfo, AccountInfo: Dropbox.AccountInfo) => {
                callback(new Common.AccountInfo(accountInfo));
            });
        }
    }

    export class ZipReader {
        constructor(private dropviewClient: DropviewClient, private dropboxReader: DropboxReader) {
            zip.workerScriptsPath = "/js/";
        }

        public readDirectory(path: string, callback?: (contents: Common.Content[]) => void) {
            var pathToZip = path.endsWith(".zip") ? path : path.substring(0, path.indexOf('.zip') + 4);
            this.readZip(path, function(entries) {
                callback && callback(entries.map(function(el) {
                    return Common.Content.ZipEntryToContent(el, path);
                }));
            });
        }

        private static lastZipRead = "";
        private static lastZipReadEntries = [];
        private static zipEntriesFilterAndReturn(path, entries, callback) {
            var matches = /^(.+\.zip)(.+)?$/gi.exec(path),
                mainPathParts = path.substring(1).split("/"),
                level = 0;
            if (matches[2]) {
                mainPathParts = matches[2].substring(1).split("/");
                level = mainPathParts.length;
            }
            var isFile = !!/.+\/.+\..+$/.test(path),
                filtered = entries.filter(function(entry) {
                    var pathParts = entry.filename.split("/");
                    if (pathParts.length > 0 && !pathParts[pathParts.length - 1]) {
                        pathParts.length--;
                    }
                    if (!isFile && level == pathParts.length - 1) {
                        for (var i = 0; i < pathParts.length - 1; i++) {
                            if (pathParts[i] !== mainPathParts[i]) {
                                return false;
                            }
                        };
                        return true;
                    } else if (isFile && level == pathParts.length) {
                        for (var i = 0; i < pathParts.length; i++) {
                            if (pathParts[i] !== mainPathParts[i]) {
                                return false;
                            }
                        };
                        return true;
                    } else {
                        return false;
                    }
                });
            callback && callback(filtered);
        }

        private readZip(path: string, callback?: (entries: any[]) => void) {
            var self = this;
            var pathToZip = path.endsWith(".zip") ? path : path.substring(0, path.indexOf('.zip') + 4);
            if (ZipReader.lastZipRead == pathToZip && ZipReader.lastZipReadEntries.length) {
                ZipReader.zipEntriesFilterAndReturn(path, ZipReader.lastZipReadEntries, callback);
            } else {
                this.dropboxReader.readBlob(path, (blob: Blob) => {
                    zip.createReader(new zip.BlobReader(blob), function(reader: zip.BlobReader) {
                        reader.getEntries(function(entries) {
                            if (entries.length) {
                                ZipReader.lastZipRead = pathToZip;
                                ZipReader.lastZipReadEntries = entries;
                                ZipReader.zipEntriesFilterAndReturn(path, ZipReader.lastZipReadEntries, callback);
                            }
                        });
                    }, function(error) {});
                });
            }
        }

        public readBlob(path: string, callback?: (blob: Blob) => void) {
            this.readZip(path, function(entry) {
                entry[0].getData(new zip.BlobWriter(), function(blob) {
                    callback && callback(blob);
                });
            });
        }

        public readArrayBuffer(path: string, callback?: (buffer: ArrayBuffer) => void) {
            var self = this;
            this.readZip(path, function(entry) {
                entry[0].getData(new zip.BlobWriter(), function(blob) {
                    var uint8ArrayNew = null;
                    var arrayBufferNew = null;
                    var fileReader = new FileReader();
                    fileReader.onload = function(progressEvent) {
                        callback && callback(this.result);
                    };
                    fileReader.readAsArrayBuffer(blob);
                });
            });
        }
    }

    export class DropviewReader {
        public zipReader: ZipReader;
        public dropboxReader: DropboxReader;

        constructor(private dropviewClient: DropviewClient) {
            this.dropboxReader = new DropboxReader(dropviewClient);
            this.zipReader = new ZipReader(dropviewClient, this.dropboxReader);
        }

        public readDirectory(path: string, callback?: (contents: Common.Content[]) => void) {
            var isZip = path.contains(".zip");
            if (isZip) {
                this.zipReader.readDirectory(path, callback);
            } else {
                this.dropboxReader.readDirectory(path, callback);
            }
        }

        public readBlob(path: string, callback?: (blob: Blob) => void) {
            var self = this;
            var isZip = path.contains(".zip");
            if (isZip) {
                this.zipReader.readBlob(path, (blob: Blob) => {
                    callback && callback(blob);
                });
            } else {
                this.dropboxReader.readBlob(path, (blob: Blob) => {
                    callback && callback(blob);
                });
            }
        }

        public readArrayBuffer(path: string, callback?: (buffer: ArrayBuffer) => void) {
            var isZip = path.contains(".zip");
            if (isZip) {
                this.zipReader.readArrayBuffer(path, (buffer: ArrayBuffer) => {
                    callback && callback(buffer);
                });
            } else {
                this.dropboxReader.readArrayBuffer(path, (buffer: ArrayBuffer) => {
                    callback && callback(buffer);
                });
            }
        }
    }

	export class DropviewClient {
        private static _client: Dropbox.Client;

        constructor(private dropboxErrorService: DropboxErrorService) {}

        public isAuthenticated(): boolean {
            return DropviewClient._client.isAuthenticated();
        }

        public authenticate(options: Dropbox.AuthenticateOptions, callback: (client: Dropbox.Client) => void) {
            var self = this;
            DropviewClient._client.authenticate((error: any, client: Dropbox.Client) => {
                self.dropboxErrorService.showError(error);
                callback(client);
            });
        }

        public signOff(callback: () => void) {
            var self = this;
            DropviewClient._client.signOff((error: ApiError) => {
                self.dropboxErrorService.showError(error);
            });
        }

        public getAccountInfo(callback: (accountInfo: Dropbox.AccountInfo, AccountInfo: Dropbox.AccountInfo) => void) {
            var self = this;
            DropviewClient._client.getAccountInfo((error: ApiError, accountInfo: Dropbox.AccountInfo, AccountInfo: Dropbox.AccountInfo) => {
                self.dropboxErrorService.showError(error);
                callback(accountInfo, AccountInfo);
            });
        }

        public writeFile(path: string, text: string, callback?: () => void) {
            var self = this;
            DropviewClient._client.writeFile(path, text, (error, stat) => {
                self.dropboxErrorService.showError(error);
                callback && callback();
            });
        }

        public readFile(path: string, callback?: (fileContents: string, data: File.Stat) => void) {
            var self = this;
            DropviewClient._client.readFile(path, (error: ApiError, fileContents: string, data: File.Stat) => {
                self.dropboxErrorService.showError(error);
                callback && callback(fileContents, data);
            });
        }

        public readBlob(path: string, callback?: (blob: Blob, data: File.Stat, rangeInfo: Dropbox.Http.RangeInfo) => void) {
            var self = this;
            DropviewClient._client.readFile(path, {
                blob: true
            }, (error: ApiError, fileContents: any, stat: File.Stat, rangeInfo: Dropbox.Http.RangeInfo) => {
                self.dropboxErrorService.showError(error);
                callback && callback(<Blob>fileContents, stat, rangeInfo);
            });
        }

        public readArrayBuffer(path: string, callback?: (buffer: ArrayBuffer, stat: File.Stat, rangeInfo: Dropbox.Http.RangeInfo) => void) {
            var self = this;
            DropviewClient._client.readFile(path, {
                arrayBuffer: true
            }, (error: ApiError, fileContents: any, stat: File.Stat, rangeInfo: Dropbox.Http.RangeInfo) => {
                self.dropboxErrorService.showError(error);
                callback && callback(<ArrayBuffer>fileContents, stat, rangeInfo);
            });
        }

        public readDirectory(path: string, callback?: (entries: string[], root: File.Stat, contents: File.Stat[]) => void) {
            var self = this;
            DropviewClient._client.readdir(path, (error: ApiError, entries: string[], root: File.Stat, contents: File.Stat[]) => {
                self.dropboxErrorService.showError(error);
                callback && callback(entries, root, contents);
            });
        }

        public getUrl(path: string, callback?: (url: File.ShareUrl) => void) {
            var self = this;
            DropviewClient._client.makeUrl(path, {
                download: true
            }, (error: ApiError, shareUrl: File.ShareUrl) => {
                self.dropboxErrorService.showError(error);
                callback && callback(shareUrl);
            });
        }

		public static get client(): Dropbox.Client {
			if(!DropviewClient._client){
				DropviewClient._client = new Dropbox.Client({
        			key: 'c4b2gh3ker8u0yv'
    			});
    			DropviewClient._client.authDriver(new Dropbox.AuthDriver.Popup({
                    redirectUrl: window.location.origin + '/templates/ouath_dropbox'
    			}));
			}
			return DropviewClient._client;
		}
	}

	export class SpinnerService {
		public static _backDrop: JQuery = $('<div class="modal-backdrop fade in"></div>').appendTo(document.body).hide();

		constructor(private usSpinnerService: any) {}

		public start() {
			this.usSpinnerService.spin('main-spinner')
		}

		public stop() {
			this.usSpinnerService.stop('main-spinner');
		}

		public startModal() {
            SpinnerService._backDrop.show();
            this.start();
		}

		public stopModal() {
			SpinnerService._backDrop.hide();
            this.stop();
		}
	}

	export class DropboxErrorService {
		private static _isOpened: boolean = false;

		public showError(error: Dropbox.ApiError): void {
			if (!error) {
                return;
            }
            var message = "";
            switch (error.status) {
                case Dropbox.ApiError.INVALID_TOKEN:
                    message = "Your user token has expired. Please refresh the page.";
                    break;
                case Dropbox.ApiError.OVER_QUOTA:
                    message = "Your Dropbox folder is full.";
                    break;
                case Dropbox.ApiError.RATE_LIMITED:
                    message = "Try again later please.";
                    break;
                case Dropbox.ApiError.NETWORK_ERROR:
                    message = "Are you connected to the internet? If no, then connect.";
                    break;
                case Dropbox.ApiError.INVALID_PARAM:
                case Dropbox.ApiError.OAUTH_ERROR:
                case Dropbox.ApiError.INVALID_METHOD:
                case Dropbox.ApiError.NOT_FOUND:
            	default:
                	message = "An error occured. Please refresh the page.";
            }
            if (!DropboxErrorService._isOpened) {
                DropboxErrorService._isOpened = true;
                bootbox.alert(message, function() {
                    DropboxErrorService._isOpened = false;
                });
            }
		}
	}

	export class DropboxActionService {
		constructor(
			private videoPlayerService: Factories.IVideoPlayerService,
            private dropviewReader: DropviewReader,
			private spinnerService: SpinnerService,
            private $modal) {}

		openText(content: Common.Content) {
            var self = this;
			this.dropviewReader.dropboxReader.readFile(content.path, function(text: string) {                
                var modalInstance = self.$modal.open({
                    windowClass: 'text-editor',
                    templateUrl: 'templates/text_editor',
                    controller: 'TextEditorController as textEditor',
                    resolve: {
                        text: function() {
                            return text;
                        },
                        content: function() {
                            return content;
                        }
                    }
                });
            });
        }
		
        openImageCarousel(content: Common.Content) {
            var self = this;
            var imagesFolder = /(.*)\/.+?\./gi.exec(content.path)[1];
            this.dropviewReader.dropboxReader.getImages(imagesFolder, (images: Common.Content[]) => {
                var modalInstance = self.$modal.open({
                    templateUrl: 'templates/image_carousel',
                    controller: 'ImageViewerController as imageViewer',
                    resolve: {
                        carouselController: function() {
                            var currentIndex = images.indexOfObject('path', content.path),
                                loadedImages = [];
                            this.next  = (urls: Common.ImageInfo[]) => {
                                if(currentIndex + 1 > images.length){
                                    currentIndex = 0;
                                } else {
                                    currentIndex++;
                                }

                                if(!loadedImages[currentIndex]){
                                    self.dropviewReader.dropboxReader.getImageUrl(images[currentIndex].path, (url: string) => {
                                        loadedImages[currentIndex] = url;
                                        urls.push(new Common.ImageInfo(url, true));
                                    });
                                }        
                            };
                            this.prev = (urls: Common.ImageInfo[]) => {
                                if(currentIndex == 0){
                                    currentIndex = images.length - 1;
                                } else {
                                    currentIndex --;
                                }

                                if(!loadedImages[currentIndex]){
                                    self.dropviewReader.dropboxReader.getImageUrl(images[currentIndex].path, (url: string) => {
                                        loadedImages[currentIndex] = url;
                                        urls.push(new Common.ImageInfo(url, true));
                                    });
                                }  
                            };
                            this.count = () => {
                                return images.length;
                            };
                            this.first = (urls: Common.ImageInfo[]) => {
                                if(!loadedImages[currentIndex]){
                                    self.dropviewReader.dropboxReader.getImageUrl(images[currentIndex].path, (url: string) => {
                                        loadedImages[currentIndex] = url;
                                        urls.push(new Common.ImageInfo(url, true));
                                    });
                                }
                            };
                            return this;
                        }
                    }
                });
            })
        }

		playAudio(content: Common.Content) {
            var self = this;
            this.dropviewReader.readArrayBuffer(content.path, (buffer: ArrayBuffer) => {
                var modalInstance = self.$modal.open({
                    windowClass: 'audioPlayer',
                    templateUrl: 'templates/audio_player',
                    controller: 'AudioPlayerController as audioPlayer',
                    resolve: {
                        arrayBuffer: function() {
                            return buffer;
                        },
                        info: function() {
                            return {
                                name: content.name
                            };
                        }
                    }
                });
            });
		}

		playVideo(content: Common.Content) {
            var self = this;
            this.dropviewReader.readBlob(content.path, (blob: Blob) => {
                self.videoPlayerService.addBlob(blob);
            });
		}
	}

    export module Factories {
    	export interface IAudioPlayerService {
    		addBuffer(buffer): void;
    		addInfo(info): void;
    		play(): void;
    		pause(): void;
    		stop(): void;
    	}

    	export interface IVideoPlayerService {
    		addBlob(blob): void;
    		addInfo(info): void;
    		play(): void;
    		pause(): void;
    	}

    	export interface IFileReaderService {
    		(path: string, callback: (contents: Common.Content[]) => void): () => void;
    	}

    	export function AudioPlayerService($rootScope: ng.IScope): IAudioPlayerService {
    		var config = <IAudioPlayerService>{};

        	config.addBuffer = function(buffer) {
            	$rootScope.$broadcast('audio-player:add-buffer', buffer);
        	};

        	config.addInfo = function(info) {
            	$rootScope.$broadcast('audio-player:add-info', info);
        	};

        	config.play = function() {
            	$rootScope.$broadcast('audio-player:play');
        	};

        	config.pause = function() {
            	$rootScope.$broadcast('audio-player:pause');
        	};

        	config.stop = function() {
            	$rootScope.$broadcast('audio-player:stop');
        	};

        	return config;
    	}

    	export function VideoPlayerService($rootScope: ng.IScope): IVideoPlayerService {
            var config = <IVideoPlayerService>{};

	        config.addBlob = function(blob) {
            	$rootScope.$broadcast('video-player:add-blob', blob);
        	};

        	config.addInfo = function(info) {
	            $rootScope.$broadcast('video-player:add-info', info);
        	};

        	config.play = function() {
            	$rootScope.$broadcast('video-player:play');
        	};

        	config.pause = function() {
            	$rootScope.$broadcast('video-player:pause');
        	};

        	return config;
    	}
    }
}