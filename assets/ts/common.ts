/// <reference path="typings/tsd.d.ts" />
module Common {
	export interface IDictionary {
	}

	export class Dictionary {
		_keys: string[] = new Array<string>();
		_values: any[] = new Array<any>();

		constructor(init: { key: string; value: any; }[]) {
			for (var x = 0; x < init.length; x++) {
				this[init[x].key] = init[x].value;
				this._keys.push(init[x].key);
				this._values.push(init[x].value);
			}
		}

		add(key: string, value: any) {
			this[key] = value;
			this._keys.push(key);
			this._values.push(value);
		}

		remove(key: string) {
			var index = this._keys.indexOf(key, 0);
			this._keys.splice(index, 1);
			this._values.splice(index, 1);

			delete this[key];
		}

		keys(): string[] {
			return this._keys;
		}

		values(): any[] {
			return this._values;
		}

		containsKey(key: string) {
			if (typeof this[key] === "undefined") {
				return false;
			}

			return true;
		}

		toLookup(): IDictionary {
			return this;
		}
	}

	export interface IPubSubHandle {
		remove: () => void;
	}

	export class PubSub {
		private static topics: { [topic: string]: any; } = {};

		public static subscribe(topic: string, listener: (info: any) => void, context: any) : IPubSubHandle {
			context = context || this;
			if(!PubSub.topics[topic]){
				PubSub.topics[topic] = {
					queue: []
				};
			}
			 
			var index = PubSub.topics[topic].queue.push({
				listener: listener,
				context: context
			}) - 1;
			
			return {
				remove: function() {
					delete PubSub.topics[topic].queue[index];
				}
			};
		}

		public static publish(topic: string, info: any) : void {
			if(!PubSub.topics[topic] || !PubSub.topics[topic].queue.length) {
				return;
			}
			var items = PubSub.topics[topic].queue;
			items.forEach(function(item) {
				item.listener.apply(item.context, [info || {}]);
			});
		}
	}

	export class Step {
		constructor(public name: string, public path: string) {}

		public static toSteps(path: string) : Array<Step> {
			var stepNames = path.split("/"),
				stepCount = stepNames.length,
				steps = new Array<Step>();
			for (var i = 0; i < stepCount; i++) {
				var path = stepNames.join("/"),
					name = stepNames.pop();
				if (name.length == 0) {
					continue;
				}
				steps.push(new Step(name, path));
			};
			return steps.reverse();
		}
	}

	export enum ContentType {
		file,
		folder,
		none
	}

	export enum DropboxIconType {
		folder,
		text,
		folderCamera,
		blank,
		archive,
		dvd,
		folderGray,
		folderPublic,
		folderStar,
		folderUser,
		folderUserGray,
		pdf,
		code,
		excel,
		film,
		gear,
		paint,
		picture,
		powerpoint,
		sound,
		word,
		linux,
		app,
		package
	}

	export interface IIconType {
		iconName: string;
		iconClass: string;
	}

	export interface IIconTypeDictionary extends IDictionary {
		[index: string]: IIconType;
	}

	export class IconTypeDictionary extends Dictionary {
		constructor(init: { key: string; value: IIconType; }[]) {
			super(init);
		}

		values(): IIconType[] {
			return this._values;
		}

		toLookup(): IconTypeDictionary {
			return this;
		}
	}

	export class ContentIconType implements IIconType {
		private static iconTypes: IconTypeDictionary = new IconTypeDictionary([
			{ key: DropboxIconType[DropboxIconType.folder], value: new ContentIconType("folder", "fa-folder-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.text], value: new ContentIconType("page_white_text", "fa-file-text-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.folderCamera], value: new ContentIconType("folder_camera", "fa-camera text-primary") },
			{ key: DropboxIconType[DropboxIconType.blank], value: new ContentIconType("page_white", "fa-file-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.archive], value: new ContentIconType("page_white_compressed", "fa-file-archive-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.dvd], value: new ContentIconType("page_white_dvd", "icon-cd text-primary") },
			{ key: DropboxIconType[DropboxIconType.folderGray], value: new ContentIconType("folder_gray", "fa-folder text-muted") },
			{ key: DropboxIconType[DropboxIconType.folderPublic], value: new ContentIconType("folder_public", "fa-globe text-primary") },
			{ key: DropboxIconType[DropboxIconType.folderStar], value: new ContentIconType("folder_star", "fa-star-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.folderUser], value: new ContentIconType("folder_user", "fa-user text-primary") },
			{ key: DropboxIconType[DropboxIconType.folderUserGray], value: new ContentIconType("folder_user_gray", "fa-user text-muted") },
			{ key: DropboxIconType[DropboxIconType.pdf], value: new ContentIconType("page_white_acrobat", "fa-file-pdf-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.code], value: new ContentIconType("page_white_code", "fa-file-code-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.excel], value: new ContentIconType("page_white_excel", "fa-file-excel-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.film], value: new ContentIconType("page_white_film", "fa-film text-primary") },
			{ key: DropboxIconType[DropboxIconType.gear], value: new ContentIconType("page_white_gear", "fa-gear text-primary") },
			{ key: DropboxIconType[DropboxIconType.paint], value: new ContentIconType("page_white_paint", "fa-paint-brush text-primary") },
			{ key: DropboxIconType[DropboxIconType.picture], value: new ContentIconType("page_white_picture", "fa-picture-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.powerpoint], value: new ContentIconType("page_white_powerpoint", "fa-file-powerpoint-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.sound], value: new ContentIconType("page_white_sound", "fa-file-sound-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.word], value: new ContentIconType("page_white_word", "fa-file-word-o text-primary") },
			{ key: DropboxIconType[DropboxIconType.linux], value: new ContentIconType("page_white_tux", "fa-linux text-primary") },
			{ key: DropboxIconType[DropboxIconType.app], value: new ContentIconType("folder_app", "icon-app text-primary") },
			{ key: DropboxIconType[DropboxIconType.package], value: new ContentIconType("package", "icon-package text-primary") }
		]).toLookup();

		constructor(public iconName: string, public iconClass: string) {
			this.iconName = iconName;
			this.iconClass = iconClass;
		}

		public static getByDropboxType(type: DropboxIconType): ContentIconType {
			return ContentIconType.iconTypes[DropboxIconType[type]];
		}

		public static getByName(iconName: string): ContentIconType {
			return ContentIconType.iconTypes.values().filter(function(el){
				return el.iconName == iconName;
			})[0];
		}

		public static toContentIconType(filetype: FileType): ContentIconType {
			if (!filetype.extension) {
				return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.blank]];
			}

			switch (filetype.type) {
				case "text":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.text]];
				case "archive":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.archive]]
				case "disk-image":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.dvd]]
				case "page-layout":
					if (filetype.extension == "pdf") {
						return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.pdf]]
					} else {
						return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.blank]]
					}
				case "excel":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.excel]]
				case "video":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.film]]
				case "raster-image":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.picture]]
				case "power-point":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.powerpoint]]
				case "audio":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.sound]]
				case "word":
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.word]]
				default:
					return ContentIconType.iconTypes[DropboxIconType[DropboxIconType.blank]]
			}
		}
	}

	export class Content {
		public type: ContentType;
		public iconType: ContentIconType;
		public name: string;
		public path: string;
		public kind: string;
		public id: string;
		public modified: Date;

		constructor(content: Dropbox.File.Stat) {
			var fileType = Common.FileType.getType(content.name);
			this.name = content.name;
			this.path = content.path;
			this.type = content.isFile ? ContentType.file : ContentType.folder;
			this.iconType = content.typeIcon ?
				ContentIconType.getByName(content.typeIcon) :
				(content.isFile ?
					ContentIconType.toContentIconType(fileType) :
					ContentIconType.getByName("folder"));
			this.kind = this.type === ContentType.file && (/[.]/.exec(content.name)) ? /[^.]+$/.exec(content.name)[0] : "--";
			this.modified = content.modifiedAt;
			this.id = UUIDjs.create().toString();
		}

		public isFolder(): boolean {
			return this.type === ContentType.folder;
		}

		public isFile(): boolean {
			return this.type === ContentType.file && !this.isText() && !this.isAudio() && !this.isVideo() && !this.isArchive() && !this.isImage();
		}

		public isText(): boolean {
			return this.iconType === ContentIconType.getByDropboxType(DropboxIconType.text) && this.type === ContentType.file;
		}

		public isAudio(): boolean {
			return this.iconType === ContentIconType.getByDropboxType(DropboxIconType.sound) && this.type === ContentType.file;
		}

		public isVideo(): boolean {
			return this.iconType === ContentIconType.getByDropboxType(DropboxIconType.film) && this.type === ContentType.file;
		}

		public isArchive(): boolean {
			return this.iconType === ContentIconType.getByDropboxType(DropboxIconType.archive) && this.type === ContentType.file;
		}

		public isImage(): boolean {
			return this.iconType === ContentIconType.getByDropboxType(DropboxIconType.picture) && this.type === ContentType.file;
		}

		public static toContents(contents: Dropbox.File.Stat[]): Content[] {
			var temp = new Array<Content>();
			for (var i = 0; i < contents.length; i++) {
				var content = new Content(contents[i]);
				temp.push(new Content(contents[i]));
			}
			return temp;
		}

		public static empty(): Content {
			var stat = new Dropbox.File.Stat();
			stat.path = "";
			stat.name = "";
			stat.modifiedAt = new Date();
			stat.isFile = true;
			var emptyContent = new Content(stat);
			emptyContent.type = ContentType.none;
			emptyContent.iconType = ContentIconType.getByName("blank");
			emptyContent.kind = "--";
			return emptyContent;
		}

		public static ZipEntryToContent(entry: any, basePath: string): Content {
			var filename = entry.filename;
			if (entry.directory) {
				filename = filename.slice(0, -1);
			}
			filename = filename.substring(filename.lastIndexOf('/') + 1);
			return new Content(Dropbox.File.Stat.parse({
				path: basePath + "/" + filename,
				name: filename,
				modifiedAt: entry.lastModDate,
				isFile: !entry.directory
			}));
		}
	}

	export class AccountQuotaInfo {
		public privateBytes: Byte;
		public quota: Byte;
		public sharedBytes: Byte;
		public usedQuota: Byte;
		public usedInPercents: number;
		public overUsed: boolean;

		constructor(quotaInfo: any) {
			this.privateBytes = Byte.toByte(quotaInfo.datastores);
			this.quota = Byte.toByte(quotaInfo.quota);
			this.sharedBytes = Byte.toByte(quotaInfo.shared);
			this.usedQuota = Byte.toByte(quotaInfo.normal);
			this.usedInPercents = parseInt(((quotaInfo.normal / quotaInfo.quota) * 100).toFixed());
			this.overUsed = this.usedInPercents >= 100;
		}
	}

	export class AccountInfo {
		public countryCode: string;
		public email: string;
		public name: string;
		public referallUrl: string;
		public uid: string;
		public quotaInfo: AccountQuotaInfo;

		constructor(accountInfo: any) {
			this.countryCode = accountInfo.countryCode;
			this.email = accountInfo.email;
			this.name = accountInfo.name;
			this.referallUrl = accountInfo.referallUrl;
			this.uid = accountInfo.uid;
			this.quotaInfo = new AccountQuotaInfo(accountInfo._json.quota_info);
		}
	}

	export enum ByteType {
		Bytes,
		KB,
		MB,
		GB,
		TB,
		PB,
		EB,
		ZB,
		YB
	}

	export class Byte {
		private static k: number = 1000;
		public name: string = ByteType[0];
		constructor(public bytes: number, public type: ByteType) {
			this.name = ByteType[this.type];
		}

		public static toByte(bytes: number): Byte {
			if (bytes == 0) {
				return new Byte(0, ByteType.Bytes);
			}
			var i = Math.floor(Math.log(bytes) / Math.log(Byte.k));
			return new Byte(parseFloat((bytes / Math.pow(Byte.k, i))
				.toPrecision(3)), ByteType[ByteType[i]]);
		}
	}

	export class FileType {
		public static fileTypes: { [extension: string]: string; } = {
			"doc": "word",
			"docx": "word",
			"wpd": "word",
			"wps": "word",
			"rtf": "word",
			"log": "text",
			"txt": "text",
			"csv": "excel",
			"xls": "excel",
			"xlsx": "excel",
			"xlr": "excel",
			"pps": "power-point",
			"ppt": "power-point",
			"pptx": "power-point",
			"aif": "audio",
			"iif": "audio",
			"m4a": "audio",
			"mid": "audio",
			"mp3": "audio",
			"mpa": "audio",
			"ra": "audio",
			"wav": "audio",
			"wma": "audio",
			"3g2": "video",
			"3gp": "video",
			"asf": "video",
			"asx": "video",
			"avi": "video",
			"flv": "video",
			"m4v": "video",
			"mov": "video",
			"mp4": "video",
			"mpg": "video",
			"rm": "video",
			"wmv": "video",
			"bmp": "raster-image",
			"dds": "raster-image",
			"gif": "raster-image",
			"jpg": "raster-image",
			"jpeg": "raster-image",
			"png": "raster-image",
			"psd": "raster-image",
			"psp": "raster-image",
			"tga": "raster-image",
			"thm": "raster-image",
			"tif": "raster-image",
			"tiff": "raster-image",
			"yuv": "raster-image",
			"ai": "vector-image",
			"eps": "vector-image",
			"ps": "vector-image",
			"svg": "vector-image",
			"apk": "executable",
			"app": "executable",
			"bat": "executable",
			"cgi": "executable",
			"com": "executable",
			"exe": "executable",
			"gadget": "executable",
			"jar": "executable",
			"pif": "executable",
			"7z": "archive",
			"cbr": "archive",
			"deb": "archive",
			"gz": "archive",
			"pkg": "archive",
			"rar": "archive",
			"rpm": "archive",
			"sitx": "archive",
			"tar.gz": "archive",
			"zip": "archive",
			"zipx": "archive",
			"bin": "disk-image",
			"cue": "disk-image",
			"dmg": "disk-image",
			"iso": "disk-image",
			"mdf": "disk-image",
			"toast": "disk-image",
			"vcd": "disk-image",
			"pdf": "page-layout",
			"djvu": "page-layout"
		};

		constructor(public type: string, public extension: string) {
		}

		public static getType(filename: string): FileType {
			var match = /[^.]+$/.exec(filename);
			if (/[.]/.exec(filename)) {
				var extension: string = /[^.]+$/.exec(filename)[0],
					fileType = FileType.fileTypes[extension];
				return new FileType(fileType ? fileType : "unknown", extension)
			} else {
				return new FileType("none", "");
			}
		}
	}

	export class ImageInfo {
		constructor(public url: string, public active: boolean) {}
	}
}