/// <reference path="typings/tsd.d.ts" />

interface URL {
	createObjectURL(blob: any): string;
}

module Directives {
	export function Breadcrumbs($state, $stateParams) {
		return {
            restrict: 'E',
            templateUrl: 'templates/breadcrumbs',
            replace: true,
            compile: function(tElement, tAttrs) {
                return function($scope, $elem, $attr) {
                    var fileTableView = $state.$current.views['filetable@dir'];
                    if(fileTableView){
                        var data = fileTableView.data,
                            isDefined = angular.isDefined(data) && angular.isDefined(data.startsWith);
                        if (isDefined) {
                            var startsWith = data.startsWith;
                            $scope.steps = [new Common.Step("Dropbox", "")].concat(Common.Step.toSteps($stateParams.path));
                        }
                    }
                }
            }
        };
	}

	export function DropboxActions(dropboxActionService: Services.DropboxActionService) {
		return {
            restrict: 'E',
            replace: true,
            templateUrl: 'templates/dropbox_actions',
            scope: {
                content: '='
            }
        };
	}

	export function DropboxAction(dropboxActionService: Services.DropboxActionService) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs, controllers) {
                var action = attrs.dropboxAction;
                element.click(function() {
                    switch (action) {
                        case 'openText':
                            dropboxActionService.openText(scope.content);
                            break;
                        case 'playAudio':
                            dropboxActionService.playAudio(scope.content);
                            break;
                        case 'playVideo':
                            dropboxActionService.playVideo(scope.content);
                            break;
                        case 'openImageCarousel':
                            dropboxActionService.openImageCarousel(scope.content);
                            break;
                    }
                });
            },
        };
	}

	export function VideoPlayer(spinnerService: Services.SpinnerService) {
        return {
            restrict: 'E',
            templateUrl: 'templates/video_player',
            scope: {},
            link: function(scope: any, element, attrs, controllers) {
                var videoPlayerModal = element.find('#video-player'),
                    videoTag = $('#video-tag');

                scope.playStatus = "stop";
                scope.play = function() {
                    (<HTMLVideoElement>videoTag[0]).play();
                    if (!scope.$$phase) {
                        scope.$apply(function() {
                            scope.playStatus = "play";
                        });
                    } else {
                        scope.playStatus = "play";
                    }
                };
                scope.pause = function() {
                    (<HTMLVideoElement>videoTag[0]).pause();
                    if (!scope.$$phase) {
                        scope.$apply(function() {
                            scope.playStatus = "pause";
                        });
                    } else {
                        scope.playStatus = "pause";
                    }
                };
                scope.$on('video-player:add-blob', function(event, blob) {
                    var objectUrl = window.URL.createObjectURL(blob);
                    videoTag.attr("src", objectUrl)
                    videoPlayerModal.modal('show');
                    spinnerService.stopModal();
                });
            }
        };
	}
}