var express = require('express'),
    path = require('path'),
    colors = require('colors'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    routes = require('./routes/index'),
    users = require('./routes/users'),
    app = express(),
    debug = require('debug')('angular-dropbox-viewer'),
    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

if (env === 'production') {
    app.get('*', function(req, res, next) {
        if (req.headers['x-forwarded-proto'] !== 'https') {
            return res.redirect(['https://', req.get('Host'), req.url].join(''));
        } else {
            next();
        }
    });
}

if(env === 'development'){
    app.use(require('connect-livereload')());
    console.log(colors.green("'connect-livereload' connected to express.js application"));
}

app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.get('/templates/:name', function(req, res) {
    var name = req.params.name;
    console.log(name);
    res.render('templates/' + name);
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (env === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

if (env === 'production') {
    app.set('port', process.env.PORT || 3000);

    var server = app.listen(app.get('port'), function() {
        debug('Express server listening on port ' + app.get('port'));
    });
}

module.exports = app;
